package com.maksim.calculator;

import com.maksim.calculator.dao.ConnectionBuilder;
import com.maksim.calculator.dao.ExpressionDao;
import com.maksim.calculator.dao.ExpressionDaoImpl;
import com.maksim.calculator.domain.Expression;
import com.maksim.calculator.service.CalculateService;
import com.maksim.calculator.service.impl.CalculatorImpl;
import com.maksim.calculator.utils.Menu;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.Statement;
import java.util.List;
import java.util.Scanner;

import static com.maksim.calculator.utils.Constants.*;

public class Main {

    CalculateService calculateService;

    ExpressionDao expressionDao = new ExpressionDaoImpl();

    public static void main(String[] args) throws Exception {
        new Main().run();
    }

    private void run() throws Exception {
        launch();
        Menu menu = new Menu();
        Scanner in = new Scanner(System.in);
        while (true) {
            switch (menu.menuChooseMode()) {
                case CALCULATE:
                    this.calculateService = new CalculatorImpl();
                    System.out.println("Please, enter your expression:");
                    String expression = in.nextLine();
                    Double result = calculateService.calculate(expression);
                    Expression e = new Expression(expression);
                    e.setResult(result);
                    expressionDao.saveExpression(e);
                    System.out.println("Amount of numbers in expression: " + countAmountOfNumbersInExpression(expression) + "\n");
                    break;
                case EXIT:
                    return;
                default:
                    System.out.println("Error! -> Illegal option. Please try again");
            }
        }
    }

    private static void launch() throws Exception {
        URL url1 = Main.class.getClassLoader().getResource("create_model.sql");
        List<String> strings1 = Files.readAllLines(Paths.get(url1.toURI()));
        String sql1 = String.join("", strings1);
        try (Connection connection = ConnectionBuilder.getConnection();
             Statement stmt = connection.createStatement()) {
            stmt.executeUpdate(sql1);
        }
    }

    public static int countAmountOfNumbersInExpression(String input) {
        int count = 0;
        boolean isPreviousDigit = false;
        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) == '.') {
                isPreviousDigit = true;
                continue;
            }
            if (Character.isDigit(input.charAt(i))) {
                if (!isPreviousDigit) {
                    count++;
                    isPreviousDigit = true;
                }
            } else {
                isPreviousDigit = false;
            }
        }
        return count;
    }

}
