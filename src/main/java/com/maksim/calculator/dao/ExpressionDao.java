package com.maksim.calculator.dao;

import com.maksim.calculator.exceptions.DaoException;
import com.maksim.calculator.domain.Expression;

public interface ExpressionDao {

    Long saveExpression(Expression expression) throws DaoException;

}
