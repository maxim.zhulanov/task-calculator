package com.maksim.calculator.dao;

import com.maksim.calculator.exceptions.DaoException;
import com.maksim.calculator.domain.Expression;
import com.maksim.calculator.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ExpressionDaoImpl implements ExpressionDao {

    private static final Logger logger = LoggerFactory.getLogger(ExpressionDaoImpl.class);

    @Override
    public Long saveExpression(Expression expression) throws DaoException {
        long result = -1L;
        try (
                Connection connection = ConnectionBuilder.getConnection();
                PreparedStatement statement = connection.prepareStatement(Constants.INSERT_EXPRESSION, new String[]{"id"})) {
            connection.setAutoCommit(false);
            try {
                statement.setString(1, expression.getExpression());
                statement.setDouble(2, expression.getResult());
                statement.executeUpdate();
                ResultSet generatedKeys = statement.getGeneratedKeys();
                if (generatedKeys.next()) {
                    result = generatedKeys.getLong(1);
                }
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw e;
            }
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
            throw new DaoException(e);
        }
        return result;
    }

}
