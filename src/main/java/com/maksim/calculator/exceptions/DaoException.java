package com.maksim.calculator.exceptions;

public class DaoException extends Exception {

    public DaoException(Throwable cause) {
        super(cause);
    }

}
