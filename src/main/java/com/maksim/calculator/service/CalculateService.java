package com.maksim.calculator.service;

public interface CalculateService {

    Double calculate(String expression);

}
