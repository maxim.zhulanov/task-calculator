package com.maksim.calculator.exceptions;

public class OperationException extends RuntimeException {

    public OperationException(String message) {
        super(message);
    }

}
