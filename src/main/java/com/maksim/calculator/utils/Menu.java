package com.maksim.calculator.utils;

import java.util.Scanner;

public class Menu {

    public int menuChooseMode() {
        System.out.println("Main menu:\n" +
                "1. Calculate expression\n" +
                "0. Exit");
        System.out.println();
        return new Scanner(System.in).nextInt();
    }

}
